A boilerplate Spring Boot MVC application has been setup with Maven.

"As an Consumer of the API, you will be able to add, retrieve, remove and update *Cars* in your application via a RESTFul API."

### The Acceptance Criteria

- Priority order is:
  1. add
  2. retrieve 
  3. remove
  4. update

- *Car* has the following attributes:
  - Id*
  - Make
  - Model
  - Year of manufacture
  - Entry Date*

- No persistent storage (in-memory is fine).